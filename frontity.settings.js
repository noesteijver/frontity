const settings = {
  "name": "frontity-test",
  "state": {
    "frontity": {
      "url": "https://frontity.noesteprojecten.nl",
      "title": "Test Frontity Blog",
      "description": "WordPress installation for Frontity development"
    }
  },
  "packages": [
    {
      "name": "@frontity/mars-theme",
      "state": {
        "theme": {
          "menu": [
            [
              "Home",
              "/"
            ],
            [
              "Blogs",
              "/blogs"
            ],
            [
              "Portfolio",
              "/portfolio-archive"
            ],
            // [
            //   "Japan",
            //   "/tag/japan/"
            // ],
            [
              "About Us",
              "/about-us/"
            ],
            // [
            //   "Contact",
            //   "/contact/"
            // ]

          ],
          "featured": {
            "showOnList": true,
            "showOnPost": true
          },
          "autoPrefetch": "in-view",
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          // "api": "https://noesteijver.nl/wp-json",
          "api": "https://frontity.noesteprojecten.nl/wp-json",
          // "api": "https://blogs.sap.nl/wp-json",
          // "api": "https://test.frontity.io/wp-json",
          "homepage": "/web-app-development",
          params: {
            per_page: 100,
          },
          postTypes: [
            {
              type: "post",
              endpoint: "posts",
              archive: "/blog",
              params: {
                per_page: 10,
              },
            },
            {
              type: "portfolio",
              endpoint: "portfolio",
              archive: "/portfolio",
            },
            {
              type: "wp_template_part",
              endpoint: "template-parts",
            },
            {
              type: "media",
              endpoint: "media",
              // archive: "/attachments"
            },
            {
              type: "team",
              endpoint: "team",
              archive: "/team",
            },
            {
              type: "diensten",
              endpoint: "diensten",
              archive: "/diensten",
            },
          ],
          taxonomies: [
            {
              taxonomy: "opdrachtgever",
              endpoint: "opdrachtgever",
              postTypeEndpoint: "portfolio",
              params: {
                per_page: 20,
                _embed: true,
              },
            },
          ]
        },
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/head-tags",
    "frontity-contact-form-7"
  ]
};

export default settings;
