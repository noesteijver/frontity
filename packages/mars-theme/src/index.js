import Theme from "./components";
import image from "@frontity/html2react/processors/image";
import iframe from "@frontity/html2react/processors/iframe";
import linkRewrite from "./processors/linkRewrite";

const getWPConfig = {
  pattern: 'getWPConfig',
  func: async ({ route, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: `/`
    });
    const option = await response.json();

    const data = state.source.get(route);
    Object.assign(data, { ...option, isWpOptionsPage: true });
  }
}

// custom handler for ACF option pages
const acfOptionsHandler = {
  // This pattern gets an `option` param from the link.
  pattern: "acf/:option",

  // Parameters extracted from the link are included in `params`.
  func: async ({ route, params, state, libraries }) => {
    // 1. Get ACF option page from REST API using `option` from `params`.
    const response = await libraries.source.api.get({
      endpoint: `/acf/v3/options/${params.option}/`,
    });
    const option = await response.json();

    // 2. Add data to `source`.
    const data = state.source.get(route);
    Object.assign(data, { ...option, isAcfOptionsPage: true });
  },
};

const menuHandler = {
  name: "menu",
  priority: 10,
  pattern: "menu/:menuName",

  func: async ({ route, params, state, libraries }) => {
    const { api } = libraries.source;
    const { id } = params;

    // 1. fetch the data you want from the endpoint page
    const response = await api.get({
      endpoint: `/wp-api-menus/v2/menu-locations/${params.menuName}/`,
    });

    // 2. get an array with each item in json format
    const items = await response.json();
    console.log(items);

    // 3. add data to source
    const currentPageData = state.source.data[route];

    Object.assign(currentPageData, {
      id,
      items,
      isMenu: true,
    });
  }
};

const marsTheme = {
  name: "@frontity/mars-theme",
  roots: {
    /**
     *  In Frontity, any package can add React components to the site.
     *  We use roots for that, scoped to the `theme` namespace.
     */
    theme: Theme,
  },
  state: {
    /**
     * State is where the packages store their default settings and other
     * relevant state. It is scoped to the `theme` namespace.
     */
    theme: {
      sourceURL: 'https://frontity.noesteprojecten.nl/',
      menu: [],
      isMobileMenuOpen: false,
      featured: {
        showOnList: true,
        showOnPost: true,
      },
      autoPrefetch: 'in-view',
      templates: ["tekst-beeld", "versje-tabel", "gewoon-wat-tekst", "formulier", "navigatiemenu"],
      mode: 'light',
      isSubmenuOpen: {},
      introBlockPosition: {
        x: 0,
        y: 0,
        wait: false,

      },
      colors: {
        light: '#fff',
        lightModerate: 'rgba(0,0,0,0.15)',
        dark: '#222',
        darkModerate: 'rgba(255,255,255,0.1)',
        transition: '.3s ease-in-out',
      }
    },
  },
  /**
   * Actions are functions that modify the state or deal with other parts of
   * Frontity like libraries.
   */
  actions: {
    theme: {
      // beforeSSR: getNameAndDescription,
      beforeSSR: async ({ state, actions }) => {
        // getNameAndDescription,

        await Promise.all(
          [
            getWPConfig,
            actions.source.fetch("acf/options"),
            actions.source.fetch("acf/identity"),
            actions.source.fetch("getWPConfig"),
            actions.source.fetch("/diensten"),
            actions.source.fetch("/team"),
            actions.source.fetch("/opdrachtgever/"),
            actions.source.fetch("menu/menu-1"),
            // actions.source.fetch("/media"),

            // Object.values(teamHandler).map(() =>
            //   actions.source.fetch("team"),
            // ),


            state.theme.templates.map((slug) =>
              actions.source.fetch(`/wp_template_part/${slug}`)
            ),
          ]
        );
      },
      beforeCSR: async ({ libraries }) => {
        libraries.html2react.processors.push(image);
      },
      toggleMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = !state.theme.isMobileMenuOpen;
      },
      closeMobileMenu: ({ state }) => {
        state.theme.isMobileMenuOpen = false;
      },
      setDarkMode: ({ state }) => {
        // console.log('darkMode!');
        state.theme.mode = 'dark';

        if (typeof window !== "undefined") {
          localStorage.setItem(
            'theme_color_mode',
            'dark'
          );
        }
      },
      setLightMode: ({ state }) => {
        // console.log('lightMode!');
        state.theme.mode = 'light';

        if (typeof window !== "undefined") {
          localStorage.setItem(
            'theme_color_mode',
            'light'
          );
        }
      },
      mouseMove: ({ state }) => ({ event }) => {
        // console.log(event);
        // event.persist();
        event.preventDefault();
        event.stopPropagation();

        if (state.theme.introBlockPosition.wait == false) {
          const mouseMoveTarget = document.querySelector('.mouseMoveTarget');
          // console.log(mouseMoveTarget);

          // state.theme.introBlockPosition.x = ((event.clientX - ((mouseMoveTarget.offsetWidth / 2)) - mouseMoveTarget.offsetLeft));
          // state.theme.introBlockPosition.y = ((event.clientY - ((mouseMoveTarget.offsetHeight / 2)) - mouseMoveTarget.offsetTop));

          state.theme.introBlockPosition.x = (mouseMoveTarget.offsetWidth / 2 + (event.clientX - mouseMoveTarget.offsetWidth));
          state.theme.introBlockPosition.y = (mouseMoveTarget.offsetHeight / 2 + (event.clientY - mouseMoveTarget.offsetHeight));

          state.theme.introBlockPosition.wait = true;

          // setting a timeout to throttle the animation somewhat
          setTimeout(function () {
              state.theme.introBlockPosition.wait = false;
              // console.log('waiting no more');
          }, 5);
        }
        // console.log(state.theme.introBlockPosition);
      },
      mouseMoveReset: ({ state }) => ({ event }) => {
        // event.persist();
        event.preventDefault();
        event.stopPropagation();

        state.theme.introBlockPosition.x = 0;
        state.theme.introBlockPosition.y = 0;
        state.theme.introBlockPosition.wait = false;
        state.theme.introBlockPosition.transition = '.3s ease-in-out';
      },
      mouseEnter: ({state}) => ({event}) => {
        // adding a bit of a transition for when the user enters the div
        state.theme.introBlockPosition.transition = '.1s ease-in-out';
        setTimeout(() => {
          // removing the transition a little bit later to make the images react to the mouse smoothly
          state.theme.introBlockPosition.transition = 'unset';
        }, 150);
      }
    },
  },
  libraries: {
    html2react: {
      /**
       * Add a processor to `html2react` so it processes the `<img>` tags
       * inside the content HTML. You can add your own processors too
       */
      processors: [image, iframe, linkRewrite],
    },
    source: {
      handlers: [acfOptionsHandler, getWPConfig, menuHandler],
    }
  },
};

export default marsTheme;
