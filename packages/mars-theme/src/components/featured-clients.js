import React, { useEffect } from "react";
import { connect, styled, css } from "frontity";
import Loading from "./loading";
// import Switch from "@frontity/components/switch";

import FeaturedClient from "./featured-client";

import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, EffectFade, Virtual } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Virtual]);

import Link from "./link";

const FeaturedClients = ({ state, actions }) => {
   const options = state.source.get("acf/options");
   const sliderItems = options.acf.featured_clients;

   let dataTerm = '';
   let dataTermImage = '';
   let termIDs = '';
   let termSlugs = '';
   let retrievedSliderItemsCount = 0;
   let retrievedImagesCount = 0;
   let imagesStringCount = 0;
   let imagesString = '';
   let imagesRetrieved = '';
   let slides = '';

   // console.log(termSlugs);
   // console.log(sliderItems);



   sliderItems.forEach((val, key) => {
      const termLink = `/${val.taxonomy}/${val.slug}`;
      // console.log(termLink);

      useEffect(() => {
         actions.source.fetch(termLink);
      }, []);

      dataTerm = state.source.get(termLink);
      // console.log(dataTerm);

      if (dataTerm.isReady) {
         retrievedSliderItemsCount++;
         // console.log(retrievedSliderItemsCount);
      }
   });

   // console.log(sliderItems);

   if (retrievedSliderItemsCount == sliderItems.length) {
      sliderItems.map((slideContent, index) => {
         // console.log(slideContent);

         // adding the image name to the imagesString string
         imagesString += state.source.opdrachtgever[slideContent.term_id].acf.logo.name;
         imagesStringCount++;

         // adding a comma to the end of every image, except the last one.
         if (! (sliderItems.length == index)) {
            imagesString += ',';
         }

      });
   }

   if (imagesStringCount == sliderItems.length) {
      return (

         <Swiper
            css={css`height: 300px !important; width:100%; max-width: 1400px; margin: 50px auto; overflow: hidden;`}
            spaceBetween={40}
            pagination={{ clickable: true }}
            grabCursor
            navigation
            // fadeEffect
            // observer
            // autoplay={{
            //    delay: 1000
            // }}
            // observeParents
            breakpoints={{
               640: {
                  loopedSlides: 2,
                  slidesPerView: 2,
                  slidesPerGroup: 1,
               },
               1024: {
                  loopedSlides: 4,
                  slidesPerView: 4,
                  slidesPerGroup: 1,
               },
            }}
         >
            { sliderItems.map((slideContent, index) => {
               // console.log(index);
               // console.log(slideContent);
               // console.log(state.source.opdrachtgever[slideContent.term_id].acf.logo.name);
               return(
               <SwiperSlide key={index}>
                  <FeaturedClient
                     imgSlug={state.source.opdrachtgever[slideContent.term_id].acf.logo.name}
                     imgid={state.source.opdrachtgever[slideContent.term_id].acf.logo.id}
                     imagesString={imagesString}
                     iteration={index}
                  />
               </SwiperSlide>);
            })}

         </Swiper>
      )
   }

   return <div css={css`max-width: 80%; margin: auto auto; height: 300px; display: flex;`}><Loading /></div>;
}

export default connect(FeaturedClients);