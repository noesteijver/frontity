import React, {useEffect} from "react";
import { connect, styled, css } from "frontity";
import Link from "./link";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {fas, faAngleDown} from "@fortawesome/free-solid-svg-icons";

/**
 * Navigation Component
 *
 * It renders the navigation links
 */
// const Nav = ({ state, actions, libraries }) => {
//   // 1. fetch data related to the post type
//   useEffect(() => {
//     actions.source.fetch("menu/menu-1");
//   });

//   // 2. get data from frontity state
//   const data = state.source.get("/wp_template_part/navigatiemenu/");
//   const Html2React = libraries.html2react.Component;

//   if (data.isReady) {
//     console.log(data);
//   }

//   return data.isReady ? (
//     <NavContainer>
//       <Html2React html={state.source[data.type][data.id].content.rendered} />
//       {state.theme.menu.map(([name, link]) => {
//         // Check if the link matched the current page url
//         const isCurrentPage = state.router.link === link;
//         return (
//           <NavItem key={name}>
//             {/* If link url is the current page, add `aria-current` for a11y */}
//             <Link link={link} aria-current={isCurrentPage ? "page" : undefined}>
//               {name}
//             </Link>
//           </NavItem>
//         );
//       })}
//     </NavContainer>
//   ) : null;
// };

const Nav = ({ state, libraries }) => {
  const { items } = state.source.get("menu/menu-1");
  // console.log(items);

  return(
    <NavContainer>
      {items.map(item => {
        const name = item.title;
        const link = libraries.source.normalize(item.url);
        const isCurrentPage = state.router.link === link;
        const childItems = item.children;


        if(item.parent === 0){ //If it has no parents, it's a top level menu item
          return (
            <NavItem key={item.ID} isSelected={isCurrentPage}>
              <Link link={link} aria-current={isCurrentPage ? "page" : undefined}>
                {name}
                {childItems.length > 0 && (
                  <FontAwesomeIcon icon={fas, faAngleDown} css={css`margin-left: 10px; max-width: .625em;`} />
                )}
              </Link>
              { //if it has children, put them out now
                childItems.length > 0 && (
                  // console.log(childItems),
                  <NavDropdown>
                    {
                      childItems.map(childItem => {
                        // console.log(item);

                        const childName = childItem.title;
                        const childLink = libraries.source.normalize(childItem.url);
                        const isChildCurrentPage = state.router.link === childLink;

                        return (
                          <NavSubItem key={childItem.ID} isSelected={isChildCurrentPage}>
                            <Link link={childLink} aria-current={isChildCurrentPage ? "page" : undefined}>
                              {childName}
                            </Link>
                          </NavSubItem>
                        );
                      })
                    }
                  </NavDropdown>
                )
              }
            </NavItem>
          );
        }
      })}
  </NavContainer>
  );
};

export default connect(Nav);

const NavContainer = styled.nav`
  list-style: none;
  display: flex;
  width: 800px;
  max-width: 100%;
  box-sizing: border-box;
  padding: 0 24px;
  margin: 0;
  overflow-x: auto;

  @media screen and (max-width: 560px) {
    display: none;
  }
`;

const NavItem = styled.div`
  padding: 0;
  margin: 0 16px;
  color: #fff;
  font-size: 0.9em;
  box-sizing: border-box;
  flex-shrink: 0;

  & > a {
    display: inline-block;
    line-height: 2em;
    border-bottom: 2px solid;
    border-bottom-color: transparent;
    /* Use for semantic approach to style the current link */
    &[aria-current="page"] {
      border-bottom-color: #fff;
    }
  }

  &:first-of-type {
    margin-left: 0;
  }

  &:last-of-type {
    margin-right: 0;
  }

  &:hover {
    > * {
      display: block;
    }
  }
`;

const NavDropdown = styled.div`
  padding: 10px;
  background-color: #1428c8;
  position: absolute;
  display: none;
  z-index: 10;
`;

const NavSubItem = styled.div`
  padding-bottom: 6px;

  &:last-child {
    padding-bottom: 0;
  }

`;
