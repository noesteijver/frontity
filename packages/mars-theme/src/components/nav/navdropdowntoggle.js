import React from "react";
import { css, connect } from "frontity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {fas, faAngleDown} from "@fortawesome/free-solid-svg-icons";

const NavDropdownToggle = ({
  state,
  actions,
  theme,
  targetid,
}) => {
  state.theme.isSubmenuOpen[targetid] = (state.theme.isSubmenuOpen[targetid] == null) ? 'closed' : state.theme.isSubmenuOpen[targetid];
  console.log(targetid);

  const toggleSubmenu = ( targetid ) => {
    state.theme.isSubmenuOpen[targetid] = (state.theme.isSubmenuOpen[targetid] == 'closed') ? 'open' : 'closed';
  }

  return (
    <span css={css`margin-left: 10px;`} submenu-open={state.theme.isSubmenuOpen[targetid]} onClick={() => toggleSubmenu(targetid)}>
      <FontAwesomeIcon icon={fas, faAngleDown} className={state.theme.isSubmenuOpen[targetid] == 'open' ? 'fa-rotate-180' : ''} />
    </span>
  )
};

export default connect(NavDropdownToggle);
