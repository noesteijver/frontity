import React, { useEffect } from "react";
import { connect, styled, css } from "frontity";
import {SwiperSlide } from 'swiper/react';
import FeaturedMedia from "./featured-media";

const FeaturedClient = ({ state, actions, imgid, imgSlug, imagesString, iteration }) => {
   // console.log(imgid);
   // console.log(iteration);

   if (iteration == 0) {
      useEffect(() => {
         actions.source.fetch(`/attachment/${imagesString}/`);
      }, []);
   }

   const imagesRetrieved = state.source.get(`/attachment/${imagesString}`);


   if (imagesRetrieved.isReady) {
      // console.log(imagesRetrieved);
      return (
         <Slide>
            <FeaturedMedia id={imgid} style="margin-top: 0;" />
         </Slide>
      );
   }

   return null;
}

export default connect(FeaturedClient);

const Slide = styled.div`
   > div {
      margin: unset;

      img {
         object-fit: contain;
         margin: unset;
      }
   }
`;