import React, {useEffect} from "react";
import { connect, styled, css } from "frontity";
import FeaturedMedia from "./featured-media";
import Link from "./link";

const Member = ({ state, actions, item, count }) => {
   // console.log(item);

   return item ? (
      // console.log(item.featured_media),
      // console.log(item),
      <MemberItem>
         <FeaturedMedia id={item.featured_media} />
         <h2 dangerouslySetInnerHTML={{ __html: item.title.rendered }} />
         <div dangerouslySetInnerHTML={{ __html: item.excerpt.rendered }} />
         <Link link={item.link}>Read more ></Link>
      </MemberItem>
   ) : null;

   return null;
}

export default connect(Member);

const MemberItem = styled.div`
   /* width: calc(100% / 3); */
   display: flex;
   flex-direction: column;
   justify-content: space-between;
   padding: 15px;
`;