import React from "react";
import { connect, styled } from "frontity";

const FooterContent = ({ state, actions, libraries }) => {
   const data = state.source.get("/wp_template_part/formulier");
   const post = state.source["wp_template_part"][data.id];

   const Html2React = libraries.html2react.Component;

   return data.isReady ? (
      <ContactForm>
         <Html2React html={post.content.rendered} />
      </ContactForm>
   ) : null;
 };

 // Connect the Header component to get access to the `state` in it's `props`
 export default connect(FooterContent);

const ContactForm = styled.div`
   max-width: 350px;
   color: white;

   .wpcf7 {
      display: flex;
      flex-direction: column;

      > * {
         order: 0;
      }

      .wpcf7-form {
         order: 2;
      }

      input:not([type=submit]),
      textarea {
         width: 100%;
         border: 2px solid white;
         padding: 6px 12px;
         background-color: transparent;
         color: white;
         margin-top: .25em;
      }
   }


`;