import React from "react";
import { connect, styled } from "frontity";

const Footer = ({ state, libraries }) => {

   const data = state.source.get("/contact");
   console.log(state.source.page[data.id]);
    const contactForm = state.source.page[data.id]
    const Html2React = libraries.html2react.Component;

    return (
      <>
        <ContactForm>
            <Html2React html={contactForm.content.rendered} />
        </ContactForm>
      </>
    );
};

export default connect(Footer);

const ContactForm = styled.div`
   // style the component here
`