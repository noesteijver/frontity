import React, { useEffect} from "react";
import { connect, styled} from "frontity";
import FeaturedProjectImages from "./featured-project-images";

const FeaturedProject = ({ state, actions }) => {
   const options = state.source.get("acf/options");
   const dataPost = null;
   let imagesString = '';
   let imagesKeys = '';
   let item = '';
   // console.log(options);

   if (options.acf.featured_project) {
      const postName = options.acf.featured_project.post_name;
      const postType = options.acf.featured_project.post_type;

      useEffect(() => {
         // No need to use `async/await` here
         actions.source.fetch(`/${postType}/${postName}/`);
      }, []);

      // The data will not exist at first, `dataPost.isReady` will be false.
      // But then, it will rerender when `actions.source.fetch` is finished.
      const dataPost = state.source.get(`/${postType}/${postName}/`);

      if (dataPost.isReady) {
         item = state.source[postType][dataPost.id];
         // console.log(item);

         if (!!item.acf && !!item.acf.intro_block_images) {

            imagesKeys = Object.keys(item.acf.intro_block_images);

            // looping over the images to retrieve their names
            imagesKeys.forEach(function (key) {
               const values = item.acf.intro_block_images[key];

               // adding the image name to the imagesString string
               imagesString += values.name;

               // adding a comma to the end of every image, except the last one.
               if (!(imagesKeys[imagesKeys.length - 1] === key)) {
                  imagesString += ',';
               }
            })
            // console.log(imagesString);
         }
      }



      return (
         <FeaturedProjectWrapper>
            {
               imagesString.length > 0 && (
                  <FeaturedProjectImages imgs={imagesString} imagesKeys={imagesKeys} item={item} />
               )
            }
         </FeaturedProjectWrapper>
      );
   }

   return null;

}

export default connect(FeaturedProject);

const FeaturedProjectWrapper = styled.div`
   position: relative;
   height: 100vmin;
   width: 100vw;
   background-color: var(--nij-color-muted);
   overflow: hidden;
`;

