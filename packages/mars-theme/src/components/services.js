import React, {useEffect} from "react";
import { Global, css, connect, styled } from "frontity";
import Service from "./service";

import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, EffectFade, Virtual } from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Virtual]);

const Services = ({ state, actions }) => {
   // 1. fetch data related to the post type
   useEffect(() => {
      actions.source.fetch("/diensten/");
   });

   // 2. get data from frontity state
   const data = state.source.get("/diensten/");

   if (data.isReady && !data.isError) {
      return (
         <ServiceWrapper>
            <h1>Diensten</h1>
            <ServiceContainer>

               <Swiper
                  css={css`height: auto !important; width:100%; max-width: 1400px; margin: 50px auto; overflow: hidden;`}
                  spaceBetween={40}
                  pagination={{ clickable: true }}
                  grabCursor
                  // fadeEffect
                  // observer
                  // autoplay={{
                  //    delay: 1000
                  // }}
                  // observeParents
                  breakpoints={{
                     640: {
                        loopedSlides: 2,
                        slidesPerView: 2,
                        slidesPerGroup: 1,
                     },
                     1024: {
                        loopedSlides: 4,
                        slidesPerView: 4,
                        slidesPerGroup: 1,
                     },
                  }}
               >

                  {
                     data.items.map(({ type, id }, index) => {
                        const item = state.source[type][id];
                        // Render one Item component for each one.
                        return (
                           <SwiperSlide key={index}>
                              <Service item={item} />
                           </SwiperSlide>
                        );
                  })}

               </Swiper>
            </ServiceContainer>
         </ServiceWrapper>
      )
   }

   if (data.isError) {
      console.log(data);
   }

   return null;
}

export default connect(Services);

const ServiceWrapper = styled.div`
   width: 70vw;
   margin: 12.5vh 0 12.5vh auto;
   padding: 24px;
   border-radius: 24px;
   background-color: var(--background-color);

   @media (max-width: 1199px) {
      width: 100%;
   }

   @media (min-width: 1600px) {
      max-width: 1200px;
   }
`

const ServiceContainer = styled.div`
   display: flex;
   flex-wrap: wrap;
   margin-left: auto;

   .slick-slider {
      /* max-width: 100%; */
      width: 100%;
   }

   .slick-initialized .slick-track {
      display: flex;
      align-items: stretch;
   }

   .slick-initialized .slick-track .slick-slide {
      display: flex;
      flex: 1;
      align-items: flex-start;
      justify-content: center;
      height: unset;

      > div {
         display: flex;
         flex: 1;
         align-self: stretch;
      }
   }
`;