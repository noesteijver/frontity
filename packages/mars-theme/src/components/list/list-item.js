import React from "react";
import { connect, styled } from "frontity";
import Link from "../link";
import FeaturedMedia from "../featured-media";

/**
 * Item Component
 *
 * It renders the preview of a blog post. Each blog post contains
 * - Title: clickable title of the post
 * - Author: name of author and published date
 * - FeaturedMedia: the featured image/video of the post
 */
const Item = ({ state, item, count }) => {
  const author = state.source.author[item.author];
  const date = new Date(item.date);

  // console.log(state);
  // console.log(item);

  // Get the featured image.
  // Get featured media ID
  const fmediaId = item.featured_media;
  // console.log("fmediaId", fmediaId);

  return (
    <article>
      {count > 0 && (<StyledHR></StyledHR>)}
      <Link link={item.link}>
        <FeaturedMedia id={item.featured_media} />
        <Title dangerouslySetInnerHTML={{ __html: item.title.rendered }} />
      </Link>

      <div>
        {/* If the post has an author, we render a clickable author text. */}
        {author && (
          <StyledLink link={author.link}>
            <AuthorName>
              By <b>{author.name}</b>
            </AuthorName>
          </StyledLink>
        )}
        <PublishDate>
          {" "}
          on <b>{date.toDateString()}</b>
        </PublishDate>
      </div>

      {/* If the post has an excerpt (short summary text), we render it */}
      {item.excerpt && (
        <Excerpt dangerouslySetInnerHTML={{ __html: item.excerpt.rendered }} />
      )}
      <StyledLink link={item.link}>
        <span>Read more ></span>
      </StyledLink>
    </article>
  );
};

// Connect the Item to gain access to `state` as a prop
export default connect(Item);

const Title = styled.h1`
  font-size: 2rem;
  color: rgba(12, 17, 43);
  margin: 0;
  padding-top: 24px;
  padding-bottom: 8px;
  box-sizing: border-box;
`;

const AuthorName = styled.span`
  // color: rgba(12, 17, 43, 0.9);
  font-size: 0.9em;
`;

const StyledLink = styled(Link)`
  border-bottom: 1px solid;
  transition: .3s ease-in-out;
  &:hover,
  &:active {
    color: #1428c8;
  }

`;

const PublishDate = styled.span`
  // color: rgba(12, 17, 43, 0.9);
  font-size: 0.9em;
`;

const Excerpt = styled.div`
  line-height: 1.6em;
  // color: rgba(12, 17, 43, 0.8);
`;

const StyledHR = styled.hr`
  margin: 25px 0 25px;
  color: #1428c8;
`;
