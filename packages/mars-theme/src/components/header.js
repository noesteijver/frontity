import React from "react";
import { connect, styled } from "frontity";
import Link from "./link";
import Nav from "./nav";
import MobileMenu from "./menu";
import Toggle from "./toggle";

const Header = ({ state }) => {
  const { name, description } = state.source.get("getWPConfig");
  // const options = state.source.get("acf");
  const options = state.source.get("acf/options");

  return (
    <>
      <Container>
        <StyledLink link="/">
          <StyledImage src={options.acf.identity.site_logo}></StyledImage>
        </StyledLink>
        <HeaderHolder>
          <StyledLink link="/">
            <Title>{name}</Title>
          </StyledLink>
          <Description dangerouslySetInnerHTML={{ __html: description }} />
          <Toggle />
        </HeaderHolder>
        <MobileMenu />
      </Container>
      <Nav />
    </>
  );
};

// Connect the Header component to get access to the `state` in it's `props`
export default connect(Header);

const StyledImage = styled.img`
  width: 100px;
  height: 100px;
  background-color: white;
`;

const Container = styled.div`
  width: 800px;
  max-width: 100%;
  box-sizing: border-box;
  padding: 24px;
  color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;

const HeaderHolder = styled.div`
 margin-left: 1em;
`;

const Title = styled.h2`
  margin: 0;
  margin-bottom: 5px;
  color: white !important;
`;

const Description = styled.h4`
  margin: 0;
  color: rgba(255, 255, 255, 0.7) !important;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  float: left;
  position: relative;
`;
