import React, { useEffect, useState } from "react";
import { connect, styled, css } from "frontity";
import FeaturedMedia from "./featured-media";
import Link from "./link";

const FeaturedProjectImages = ({ state, actions, imgs, imagesKeys, item }) => {
   useEffect(() => {
      actions.source.fetch(`/media/${imgs}`);
   }, []);

   const img = state.source.get(`/media/${imgs}`);

   // const mouseMove = (event) => {
   //    // console.log(state);
   //    // event.persist();
   //    event.preventDefault();
   //    event.stopPropagation();

   //    if (state.theme.introBlockPosition.wait == false) {
   //       const mouseMoveTarget = document.querySelector('.mouseMoveTarget');
   //       // console.log(mouseMoveTarget);

   //       state.theme.introBlockPosition.x = ((event.clientX - ((mouseMoveTarget.offsetWidth / 2) - mouseMoveTarget.offsetLeft)) * 0.075);
   //       state.theme.introBlockPosition.y = ((event.clientY - ((mouseMoveTarget.offsetHeight / 2) - mouseMoveTarget.offsetTop)) * 0.075);

   //       state.theme.introBlockPosition.wait = true;

   //       // setting a timeout to throttle the animation somewhat
   //       setTimeout(function () {
   //          state.theme.introBlockPosition.wait = false;
   //          // console.log('waiting no more');
   //       }, 25);
   //    }
   //    // console.log(state.theme.introBlockPosition);
   // }

   // setting up an array with multipliers
   const imageProperties = {
      'back_parallax_layer': {
         'multiplier': 0.025,
         'size': '110%',
      },
      'middle_parallax_layer': {
         'multiplier': 0.05,
         'size': '110%',
      },
      'front_parallax_layer': {
         'multiplier': 0.075,
         'size': '110%',
      },
      'static_layer': {
         'multiplier': -0.0075,
         'size': '85%',
      },
      'top_static_layer': {
         'multiplier': -0.05,
         'size': '90%',
      },
   };

   const { mouseMove, mouseMoveReset, mouseEnter } = actions.theme;

   return (img.isReady ? (
      <FeaturedProjectImagesWrapper
      onMouseMove={(event) => { mouseMove({ event: event }) }}
      onMouseLeave={(event) => { mouseMoveReset({ event: event }) }}
      onMouseEnter={(event) => { mouseEnter({ event: event }) }}
      className="mouseMoveTarget">
         <FeaturedProjectInfo>
            <h1>Bureau voor creatie & web</h1>
            <h2 dangerouslySetInnerHTML={{ __html: 'Uitgelicht: ' + item.title.rendered }} />
            <Link link={item.link}>Read more</Link>
         </FeaturedProjectInfo>

         {

            imagesKeys.map(key => {


               return (

                  <StyledImage key={key}
                     id={item.acf.intro_block_images[key].id}
                     multiplier={imageProperties[key].multiplier}
                     positionX={state.theme.introBlockPosition.x}
                     positionY={state.theme.introBlockPosition.y}
                     size={imageProperties[key].size}
                     transition={state.theme.introBlockPosition.transition}
                  />
               )
            })
         }
      </FeaturedProjectImagesWrapper>
   ) : null);
}

export default connect(FeaturedProjectImages);

const FeaturedProjectImagesWrapper = styled.div`
   width: 100%;
   height: 100%;

   &:hover {
      > div {
         transition-duration: ${({transition}) => transition} !important;
      }
   }
`;

const StyledImage = styled(FeaturedMedia)`
   position: absolute;
   height: ${({ size }) => size};
   width: ${({ size }) => size};
   /* transform: translate(${state.theme.introBlockPosition.x * imageProperties[key].multiplier}px, ${state.theme.introBlockPosition.y * imageProperties[key].multiplier}px); */
   transform: translate(${({ positionX, multiplier }) => positionX * multiplier}px, ${({ positionY, multiplier }) => positionY * multiplier}px);
   will-change: transform;
   transition: ${({transition}) => transition};
   transition-property: transform;
   margin-top: auto;
   margin-bottom: auto;
   margin-left: auto;
   margin-right: auto;
   top: 0;
   bottom: 0;
   left: 0;
   right: 0;
`;

const FeaturedProjectInfo = styled.div`
   padding: 30px;
   position: absolute;
   top: 50%;
   transform: translateY(-50%);
   right: 30px;
   z-index: 980;
   background-color: var(--nij-color-muted);
   backdrop-filter: blur(2px);
`;

const StyledLink = styled(Link)`
  border-bottom: 1px solid;
  transition: .3s ease-in-out;
  &:hover,
  &:active {
    color: #1428c8;
  }

`;