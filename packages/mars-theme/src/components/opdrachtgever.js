import React, { useEffect } from "react";
import { connect, styled, css } from "frontity";
import FeaturedMedia from "./featured-media";

const opdrachtgever = ({ actions, state, id }) => {
   const opdrachtgever = state.source.opdrachtgever[id];

   console.log(opdrachtgever);

   if (!!opdrachtgever.acf && !!opdrachtgever.acf.logo) {
      useEffect(() => {
         actions.source.fetch(`/media/${opdrachtgever.acf.logo.name}`);
      }, []);

      state.source.get(`/media/${opdrachtgever.acf.logo.name}`);
   }



   return (
      <Opdrachtgever>
         {opdrachtgever.acf && opdrachtgever.acf.logo && (
            <FeaturedMedia id={opdrachtgever.acf.logo.id} css={css`height: unset;`} />
         )}
         <span>{opdrachtgever.name}</span>
      </Opdrachtgever>
   )
};

export default connect(opdrachtgever);

const Opdrachtgever = styled.div`
img {
   max-height: 300px;
   width: auto;
}
`;