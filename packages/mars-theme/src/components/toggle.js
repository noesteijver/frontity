import React, { useEffect } from "react";
import { connect, styled } from "frontity";

let firstTime = true;


const Toggle = ({ actions, state }) => {

   const { setLightMode, setDarkMode } = actions.theme;
   let { mode } = state.theme;

   // console.log('firstTime: ' + firstTime);

   useEffect(() => {
      if (firstTime == true) {
         let userColorMode = window.localStorage.theme_color_mode;
         let prefersDark = typeof window !== "undefined" ? window.matchMedia('(prefers-color-scheme: dark)').matches : true;
         // console.log('userColorMode: ' + userColorMode);

         if (userColorMode !== undefined) {
            prefersDark = (userColorMode == 'dark') ? true : false;
         }

         prefersDark ? setDarkMode() : setLightMode();
         firstTime = false;
      }
   });

   return (
      <Container>
         <ButtonsStyled isSelected={ mode === 'dark' } onClick={setDarkMode}>Dark</ButtonsStyled>
         <ButtonsStyled isSelected={ mode === 'light'} onClick={setLightMode}>Light</ButtonsStyled>
      </Container>
   )
}

export default connect(Toggle);

const Container = styled.div`
    display: flex;
`;

const ButtonsStyled = styled.button`
   margin-top: 10px;
   padding: 10px;
   border: 0;
   background-color: ${({ isSelected }) => (isSelected ? '#ccc' : '#fff')};
   cursor: pointer;
`;