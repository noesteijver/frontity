import React from "react";
import { Global, css, connect, styled, Head } from "frontity";
import Switch from "@frontity/components/switch";
import Header from "./header";
import Frontpage from "./frontpage";
import List from "./list";
import Post from "./post";
import Loading from "./loading";
import Title from "./title";
import PageError from "./page-error";
import FooterContent from "./footer";

// import Gutenberg css
import gutenbergThemeCSS from "./theme.min.css";
import gutenbergStyleCSS from "./style.min.css";

// import Swiper styles
import swiperCSS from 'swiper/swiper-bundle.min.css';

// import Contact from "./contact";

/**
 * Theme is the root React component of our theme. The one we will export
 * in roots.
 */
const Theme = ({ state }) => {

  // Get information about the current URL.
  const data = state.source.get(state.router.link);
  const { description } = state.source.get("getWPConfig");
  const options = state.source.get("acf/options");

  const { mode } = state.theme;

  return (
    <>
      {/* Add some metatags to the <head> of the HTML. */}
      <Title />
      <Head>
        <meta name="description" content={description} />
        <html lang="en" />
        <link  rel="preload" as="image" href={options.acf.identity.site_logo} />

      </Head>

      {/* Add some global styles for the whole site, like body or a's.
      Not classes here because we use CSS-in-JS. Only global HTML tags. */}

      <Global styles={globalStyles} />
      <Global styles={css([swiperCSS])} />

      <Global styles={css`
        :root {
          --background-color: ${mode === 'light' ? state.theme.colors.lightModerate : state.theme.colors.darkModerate};
          --nij-color-muted: rgb(20, 40, 200, .25);
        }
        body {
          background-color: ${mode === 'light' ? '#fff': '#222'};
          color: ${mode === 'light' ? '#222' : '#fff'}
        }

        h1, h2, h3, h4, h5, h6 {
          color: ${mode === 'light' ? '#222': '#fff'} !important;
        }` } />

      {/* Add the header of the site. */}
      <HeadContainer>
        <Header />
      </HeadContainer>

      {/* Add the main section. It renders a different component depending
      on the type of URL we are in. */}
      <Main>
        <Switch>
          <Loading when={data.isFetching} />
          <Frontpage when={data.isHome} />
          <List when={data.isArchive} />
          <Post when={data.isPostType && !data.isHome} />
          <PageError when={data.isError} />
        </Switch>
      </Main>
      <Footer>
        <InnerFooter>
          {/* <FooterContent/> */}
        </InnerFooter>
        <Global styles={css([gutenbergThemeCSS, gutenbergStyleCSS])} />
      </Footer>
    </>
  );
};

export default connect(Theme);

const globalStyles = css`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,"Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    transition: .3s ease-in-out;
    transition-property: background-color;
  }
  a,
  a:visited {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }

  .wpcf7-submit {
    color: white;
    appearance: none;
    border: 2px solid white;
    padding: 6px 12px;
    background: transparent;
    font-size: 1.2em;
    cursor: pointer;

    &:hover,
    &:active {
      color: #1428c8;
      background-color: white;

    }
  }

  .wp-block-gallery {
    .blocks-gallery-grid {
      width: 100%;
      .blocks-gallery-item {
        figure {
          span {
            width:100%;
          }
        }
      }
    }
  }

  @media (min-width: 800px) {
    .wp-block-image.alignfull {
      margin-left: calc(-100vw / 2 + 100% / 2);
      margin-right: calc(-100vw / 2 + 100% / 2);
      max-width: 100vw;
    }
    .wp-block-image.alignfull img {
      width: 100vw !important;
      max-width: unset;
    }
  }

`;

const HeadContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #1428c8;
`;

const Main = styled.div`
  display: flex;
  justify-content: center;
  background-image: linear-gradient(
    180deg,
    rgba(66, 174, 228, 0.1),
    rgba(66, 174, 228, 0)
  );
`;

const Footer = styled.footer`
  background-color: #1428c8;
  display: flex;
  justify-content: center;
`;

const InnerFooter = styled.div`
  display: flex;
  max-width: 800px;
  width: 100%;
  justify-content: flex-start;
  padding: 24px;
`;
