import React from "react";
import { styled, connect } from "frontity";
import Link from "./link";
import NavDropdownToggle from "./nav/navdropdowntoggle";

// const MenuModal = ({ state, actions }) => {
//   const { menu } = state.theme;
//   const isThereLinks = menu != null && menu.length > 0;

//   return (
//     <>
//       <MenuOverlay />
//       <MenuContent as="nav">
//         {isThereLinks &&
//           menu.map(([name, link]) => (
//             <MenuLink
//               key={name}
//               link={link}
//               aria-current={state.router.link === link ? "page" : undefined}
//             >
//               {name}
//             </MenuLink>
//           ))}
//       </MenuContent>
//     </>
//   );
// };

const MenuModal = ({ state, actions, libraries }) => {
  const { menu } = state.theme;
  const isThereLinks = menu != null && menu.length > 0;
  const { items } = state.source.get("menu/menu-1");

  return (
    <>
      <MenuOverlay />
      <MenuContent as="nav">
        {items.map(item => {
          const name = item.title;
          const link = libraries.source.normalize(item.url);
          const isCurrentPage = state.router.link === link;
          const childItems = item.children;


          if (item.parent === 0) { //If it has no parents, it's a top level menu item
            const NavDropdownToggleClick = (e) => {
              e.preventDefault();
              console.log(e);
            }

            return (
              <MenuWrapper key={item.ID} isSelected={isCurrentPage}>
                <Link link={link} aria-current={isCurrentPage ? "page" : undefined}>
                  {name}
                </Link>
                { //if it has children, put them out now
                  childItems.length > 0 && (
                    <>
                      <NavDropdownToggle targetid={item.ID} />
                      <NavDropdown>
                        {
                          childItems.map(childItem => {
                            // console.log(item);

                            const childName = childItem.title;
                            const childLink = libraries.source.normalize(childItem.url);
                            const isChildCurrentPage = state.router.link === childLink;

                            return (
                              <NavSubItem key={childItem.ID} isSelected={isChildCurrentPage}>
                                <Link link={childLink} aria-current={isChildCurrentPage ? "page" : undefined}>
                                  {childName}
                                </Link>
                              </NavSubItem>
                            );
                          })
                        }
                      </NavDropdown>
                    </>
                  )
                }
              </MenuWrapper>
            );
          }
        })}
      </MenuContent>
    </>
  );
};

const MenuOverlay = styled.div`
  background-color: #1f38c5;
  width: 100vw;
  height: 100vh;
  overflow: hidden auto;
  position: fixed;
  z-index: 2;
  top: 0;
  left: 0;
`;

const MenuContent = styled.div`
  z-index: 3;
  position: absolute;
  left: 0;
  right: 0;
`;

const MenuWrapper = styled.div`
  width: 100%;
  display: inline-block;
  outline: 0;
  font-size: 20px;
  text-align: center;
  padding: 1.2rem 0;

  &:hover,
  &:focus {
    background-color: rgba(0, 0, 0, 0.05);
  }
  /* styles for active link */
  &[aria-current="page"] {
    color: yellow;
    font-weight: bold;
    /* border-bottom: 4px solid yellow; */
  }

  span{
    cursor: pointer;

    &[submenu-open="closed"] {
      + * {
        display: none;
      }
    }
  }
`;

const NavItem = styled.div`
  padding: 0;
  margin: 0 16px;
  color: #fff;
  font-size: 0.9em;
  box-sizing: border-box;
  flex-shrink: 0;

  & > a {
    display: inline-block;
    line-height: 2em;
    border-bottom: 2px solid;
    border-bottom-color: transparent;
    /* Use for semantic approach to style the current link */
    &[aria-current="page"] {
      border-bottom-color: #fff;
    }
  }

  &:hover {
    > * {
      display: block;
    }
  }
`;

const NavDropdown = styled.div`
  padding: 10px;
  position: relative;
`;

const NavSubItem = styled.div`
  padding-bottom: 6px;

  &:last-child {
    padding-bottom: 0;
  }

`;


export default connect(MenuModal);
