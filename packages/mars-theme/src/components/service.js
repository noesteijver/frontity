import React, {useEffect} from "react";
import { connect, styled, css } from "frontity";
import FeaturedMedia from "./featured-media";
import Title from "./title";
import Link from "./link";

const Service = ({ state, actions, item, count }) => {
   // console.log(item);

   return item ? (
      // console.log(item.featured_media),
      // console.log(item),
      // console.log(fmediaid),
      <ServiceItem>
         <FeaturedMedia id={item.featured_media} css={css`flex: 0 !important;`} className="dienst-image" />
         <h2 dangerouslySetInnerHTML={{ __html: item.title.rendered }} />
         <div dangerouslySetInnerHTML={{ __html: item.excerpt.rendered }} />
         <Link link={item.link}>Read more ></Link>
      </ServiceItem>
   ) : null;

   return null;
}

export default connect(Service);

const ServiceItem = styled.div`
   /* width: calc(100% / 3); */
   /* width: 100%; */
   display: flex;
   flex-direction: column;
   justify-content: flex-start;
   padding: 15px;

   a {
      margin-top: auto;
   }

   .dienst-image {
      background-color: var(--background-color);
   }
`;