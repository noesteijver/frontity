import React from "react";
import { connect, styled } from "frontity";
import FrontpageSlider from './slider';
import Team from './team';
import Services from './services';
import FeaturedProject from './featured-project';
import FeaturedClients from './featured-clients';


const Front = ({ state, actions, libraries }) => {
  const options = state.source.get("acf/identity");
  // console.log(options);

  // Get information about the current URL.
  const data = state.source.get('/');
  // Get the data of the post.
  const post = state.source[data.type][data.id];

  console.log(data);

  // Get the html2react component.
  const Html2React = libraries.html2react.Component;


  return data.isReady ? (
    <FrontPage>
      <FeaturedProject />
      <FeaturedClients />
      <FrontpageSlider />
      <Services />
      <Team />

      <PageContent>
        <Title dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
        <Html2React html={post.content.rendered} />
      </PageContent>
    </FrontPage>


  ) : null;
};

export default connect(Front);

const FrontPage = styled.div`
  list-style: none;
  display: flex;
  flex-direction: column;
  /* width: 800px; */
  max-width: 100%;
  width: 100%;
  box-sizing: border-box;
  margin: 0;
  overflow-x: auto;

  .page-content {
    max-width: 800px;

    @media screen and (min-width: 1600px) {
      max-width: 1600px;
    }
  }

  @media screen and (max-width: 560px) {
    // display: none;
  }


`;

const Title = styled.h1`
  margin: 0;
  margin-top: 24px;
  margin-bottom: 8px;
  color: rgba(12, 17, 43);
`;

const PageContent = styled.div`
  max-width: 800px;
  margin: 100px auto;
  padding: 24px;
  background-color: var(--background-color);
`;