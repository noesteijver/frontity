import React, {useEffect} from "react";
import { Global, connect, styled, css } from "frontity";
import Member from "./member";

import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, EffectFade, Virtual } from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Virtual]);

const Team = ({ state, actions, libraries }) => {
   // 1. fetch data related to the post type
   useEffect(() => {
      actions.source.fetch("/team/");
   });

   // 2. get data from frontity state
   const data = state.source.get("/team/");

   if (data.isReady && !data.isError) {
      return (
         <TeamWrapper>
            <h1>Team</h1>
            <MemberContainer>

               <Swiper
                  css={css`height: auto !important; width:100%; max-width: 1400px; margin: 50px auto; overflow: hidden;`}
                  spaceBetween={40}
                  pagination={{ clickable: true }}
                  grabCursor
                  // fadeEffect
                  // observer
                  // autoplay={{
                  //    delay: 1000
                  // }}
                  // observeParents
                  breakpoints={{
                     640: {
                        loopedSlides: 2,
                        slidesPerView: 2,
                        slidesPerGroup: 1,
                     },
                     1024: {
                        loopedSlides: 3,
                        slidesPerView: 3,
                        slidesPerGroup: 1,
                     },
                  }}
               >

                  {
                     data.items.map(({ type, id }, index) => {
                        const item = state.source[type][id];
                        // Render one Item component for each one.
                        return (
                           <SwiperSlide key={index}>
                              <Member item={item} />
                           </SwiperSlide>
                        );
                  })}

               </Swiper>
            </MemberContainer>
         </TeamWrapper>
      )
   }

   if (data.isError) {
      console.log(data);
   }

   return null;
}

export default connect(Team);

const TeamWrapper = styled.div`
   width: 70vw;
   margin: 12.5vh auto 12.5vh auto;
   padding: 24px;
   border-radius: 24px;
   background-color: var(--background-color);

   @media (max-width: 1199px) {
      width: 100%;
   }

   @media (min-width: 1600px) {
      max-width: 1200px;
   }
`

const MemberContainer = styled.div`
   display: flex;
   flex-wrap: wrap;
   margin-left: auto;
`;