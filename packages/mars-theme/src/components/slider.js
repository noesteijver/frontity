import React, { useEffect } from "react";
import {css, connect, styled } from "frontity";
import FeaturedMedia from "./featured-media";
import Link from "./link";

import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, EffectFade, Virtual, Autoplay } from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Virtual, Autoplay]);

const FrontpageSlider = ({ state, actions }) => {
   const options = state.source.get("acf/options");
   const sliderItems = options.acf.home_slider_items;
   let slideCounter = 0;

   const slides = [];
   for (let i = 0; i < sliderItems.length; i++) {
      const postLinkPrefix = (sliderItems[i].post_type != 'post') ? '/' + sliderItems[i].post_type : '';
      const postLink = postLinkPrefix + "/" + sliderItems[i].post_name;

      // console.log(postLink);
      // console.log(sliderItems[i]);
      // console.log(sliderItems[i].post_type);
      // console.log(sliderItems[i].ID);
      // console.log(sliderItems[i].featured_media);

      useEffect(() => {
         actions.source.fetch(postLink);
      }, []);

      const dataPost = state.source.get(postLink);
      const featuredImageID = (dataPost.isReady) ? state.source[sliderItems[i].post_type][sliderItems[i].ID].featured_media : null;


      if (dataPost.isReady) {
         slideCounter++;
      }
      slides.push(<Slide
         link={postLinkPrefix + "/" + sliderItems[i].post_name}
         backgroundColor={state.theme.mode == 'light' ? state.theme.colors.dark : state.theme.colors.light}
         color={state.theme.mode == 'light' ? state.theme.colors.light : state.theme.colors.dark}
      ><span>slide n°{i}: {sliderItems[i].post_title}</span><FeaturedMedia id={featuredImageID} style="margin-top: 0;" /></Slide>)
   }

   if (sliderItems.length == slideCounter) {
      return (
         <FrontpageSliderWrapper
         backgroundColor={state.theme.mode == 'light' ? state.theme.colors.dark : state.theme.colors.light}
         color={state.theme.mode == 'light' ? state.theme.colors.light : state.theme.colors.light}>
            <Swiper
               css={css`height: auto !important; width:100%; max-width: 1400px; margin: 50px auto; overflow: hidden;`}
               spaceBetween={40}
               pagination={{ clickable: true }}
               grabCursor
               slidesPerView={1}
               // fadeEffect
               // observer
               navigation
               autoplay={{
                  delay: 3000
               }}
               // observeParents
            >
               {
                  slides.map((slideContent, index) => {
                     return (
                        <SwiperSlide key={index}>
                           {slideContent}
                        </SwiperSlide>
                     );
               })}
            </Swiper>
         </FrontpageSliderWrapper>
      )
   }

   return null;
}

export default connect(FrontpageSlider);

const Slide = styled(Link)`
   cursor: grab;
   overflow: hidden;
   width: 100%;
   display: block;
   position: relative;

   span {
      position: absolute;
      top: 0;
      left: 0;
      z-index: 10;
      background-color: ${({ color }) => color};
      color: ${({ backgroundColor }) => backgroundColor};
      transition: .3s ease-in-out;
      transition-property: background-color, color;
      padding: 15px;
   }

   > div {
      margin-top: 0;

      img {
         transition: .3s ease-in-out;
      }
   }

   &:hover {
      img {
         transform: scale(1.1);
      }
      span {
         text-decoration: underline;
      }
   }
`;

const FrontpageSliderWrapper = styled.div`
   margin: 20px 0;
   height: 300px;
   width: 100%;
   max-width: 1000px;
   margin: 25px auto;
`;